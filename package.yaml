name: versions
version: '3.5.1'
synopsis: Types and parsers for software version numbers.
description: 'A library for parsing and comparing software version numbers.
             We like to give version numbers to our software in a myriad of
             ways. Some ways follow strict guidelines for incrementing and comparison.
             Some follow conventional wisdom and are generally self-consistent.
             Some are just plain asinine. This library provides a means of parsing
             and comparing /any/ style of versioning, be it a nice Semantic Version
             like this:


             > 1.2.3-r1+git123


             ...or a monstrosity like this:


             > 2:10.2+0.0093r3+1-1


             Please switch to <http://semver.org Semantic Versioning> if you
             aren''t currently using it. It provides consistency in version
             incrementing and has the best constraints on comparisons.'

category: Data
author: Colin Woodbury
maintainer: colin@fosskers.ca
license: BSD3
homepage: https://gitlab.com/fosskers/versions

extra-source-files:
  - CHANGELOG.md
  - README.md

ghc-options:
  - -Wall
  - -Wincomplete-record-updates
  - -Wincomplete-uni-patterns
  - -Wredundant-constraints

dependencies:
  - base >= 4.8 && < 4.14
  - megaparsec >= 7
  - text >= 1.2

library:
  exposed-modules:
    - Data.Versions
  other-modules: []
  dependencies:
    - deepseq >= 1.4
    - hashable >= 1.2

tests:
  versions-test:
    main: Test.hs
    source-dirs: test
    other-modules: []
    ghc-options:
      - -threaded
      - -with-rtsopts=-N
    dependencies:
      - base-prelude
      - checkers >= 0.4
      - microlens >= 0.4
      - QuickCheck >= 2.9
      - tasty >= 0.10.1.2
      - tasty-hunit >= 0.9.2
      - tasty-quickcheck >= 0.8
      - versions
